# devopscamp-elasticsearch

This repository is based on this post:

https://medium.appbase.io/scaling-an-elasticsearch-cluster-with-kubernetes-120991de1451

Whereas the Prometheus repository deploys from the externally-hosted Helm chart, we're downloading the Elasticsearch manifests locally for two reasons
1. They're from a less-reputable source
1. I was having gateway/caching issues pulling them from Gitlab

- This deployment does not use the Nginx ingress.
  - Kubernetes ingress does not support TCP and UDP services
  - [Nginx ingress does](https://kubernetes.github.io/ingress-nginx/user-guide/exposing-tcp-udp-services/), but it's a bit convoluted
  - The default nginx-ingress controller only opens ports 80 and 443 on the load balancer

---

### Deployment

Gitlab CI should deploy this to your cluster with your previously-set group-level environment variables.

- Wait for the service to come up with an IP address
  - `kubectl get svc --watch`
- When it's done, you can check on Elasticsearch here
  - `ES_IP=$(kubectl get svc elasticsearch -o jsonpath='{.status.loadBalancer.ingress[].ip}')`
  - `xdg-open http://${ES_IP}:9200/_cluster/health?pretty`
